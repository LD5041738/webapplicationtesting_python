from pageObject.LoginPage import LoginPage
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen

#pytest -v -s testCases/test_login.py --browser Firefox
#pytest -v -s -n=2 testCases/test_login.py --browser Chrome . This will execuite 2 tc in parallel with help of pytest-xdist
#pytest -v -n=2 --html=Reports\report.html testCases/test_login.py --browser Chrome .Don't add -s it wont add logs if included.
class TestLogin01:

    base_url = ReadConfig.get_application_url()
    username = ReadConfig.get_application_username()
    password = ReadConfig.get_application_password()
    logger = LogGen.get_logger("TestLogin01")

    def test_verify_login_page(self, setup):
        self.driver = setup
        self.driver.get(self.base_url)
        self.logger.info("URL hit :"+self.base_url)
        actual_title = self.driver.title
        if actual_title == "Your store. Login":
            self.driver.close()
            self.logger.info("Title verified")
            assert True
        else:
            self.driver.save_screenshot(".\\Screenshots\\"+"Failed_test_verify_login_page.png")
            self.driver.close()
            self.logger.info("Title not verified")
            assert False

    def test_login_action(self, setup):
        self.driver = setup
        self.driver.get(self.base_url)
        self.logger.info("URL hit :" + self.base_url)
        lp = LoginPage(self.driver)
        lp.set_username(self.username)
        lp.set_password(self.password)
        lp.click_login_button()
        actual_title = self.driver.title
        if actual_title == "Dashboard / nopCommerce administration":
            self.driver.close()
            self.logger.info("Title verified")
            assert True
        else:
            self.driver.save_screenshot(".\\Screenshots\\" + "Failed_test_login_action.png")
            self.driver.close()
            self.logger.info("Title not verified")
            assert False

