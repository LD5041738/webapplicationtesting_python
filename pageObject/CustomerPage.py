from pageObject.BasePage import BasePage
from utilities.customLogger import LogGen
from elements.SelectElements import SelectElements
from elements.ClickOnElements import ClickOnElements
from elements.InputElements import InputElements
from elements.RadioButton import RadioButton


class CustomerPage(BasePage):
    logger = LogGen.get_logger("Customer_Page")

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver
        self.login_page_locators = self.pageLocators('CustomerPage')

    #Click on Add New Button
    def click_add_new_button(self, button_name):
        click_on_element = ClickOnElements(self.driver)
        base_page = BasePage(self.driver)
        web_element = base_page.get_element(self.locator(self.login_page_locators, 'add_new_botton'))
        click_on_element.click_on_element(web_element, button_name)

    def set_value(self, field_name, actual_value):
        input_element = InputElements(self.driver)
        web_element = self.driver.find_element_by_xpath(
            "//div[@class='form-group row']//div//label[contains(text(),'" + field_name + "')]/../../following-sibling"
                                                                                          "::div//input")
        input_element.enter_value_in_textbox(web_element, field_name, actual_value)

    def select_radio_button(self, actual_value):
        radio_button = RadioButton(self.driver)
        web_element = self.driver.find_element_by_xpath(
            "//div[@class='form-group row']//div//label[contains(text(),"
            "'Gender')]/../../following-sibling::div//input[@type='radio']/following-sibling::label[contains(text(),"
            "'" + actual_value + "')]")
        radio_button.select_radio_button(web_element, actual_value)
