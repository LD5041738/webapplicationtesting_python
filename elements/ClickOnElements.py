import time
from utilities.customLogger import LogGen
from utilities.commonUtils import CommonUtils
from utilities.Screenshots import Screenshots

class ClickOnElements:
    logger = LogGen.get_logger("ClickOnElement")

    def __init__(self, driver):
        self.driver = driver
        self.takescreenshot = Screenshots(self.driver)

    def click_on_element(self, web_element, button_name):
        try:
            common_utils = CommonUtils(self.driver)
            element_present = common_utils.check_presence_of_element(web_element)
            if element_present:
                web_element.click()
                self.logger.info(f"{button_name} is clicked")
                self.takescreenshot.take_screenshot(f"{button_name} is clicked")
                time.sleep(5)
            else:
                time.sleep(5)
                web_element.click()
                self.logger.info(f"{button_name} is clicked")
                self.takescreenshot.take_screenshot(f"{button_name} is clicked")
                time.sleep(5)
        except Exception as e:
            self.logger.info(f"Exception Occurred in ClickOnElements : {str(e.__cause__)}")
            self.takescreenshot.take_screenshot("Exception Occurred in ClickOnElements")


