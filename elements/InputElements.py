import time
from utilities.customLogger import LogGen
from utilities.commonUtils import CommonUtils
from utilities.Screenshots import Screenshots

class InputElements:
    logger = LogGen.get_logger("InputElements")

    def __init__(self, driver):
        self.driver = driver
        self.takescreenshot = Screenshots(driver)

    def enter_value_in_textbox(self, web_element, field_name, actual_value):
        try:
            common_utils = CommonUtils(self.driver)
            element_present = common_utils.check_presence_of_element(web_element)
            if element_present:
                web_element.send_keys(actual_value)
                self.logger.info(f"{actual_value} value is set to {field_name} ")
                self.takescreenshot.take_screenshot(f"{actual_value} value is set to {field_name} ")
                time.sleep(5)
            else:
                time.sleep(5)
                web_element.send_keys(actual_value)
                self.logger.info(f"{actual_value} value is set to {field_name} ")
                self.takescreenshot.take_screenshot(f"{actual_value} value is set to {field_name} ")
                time.sleep(5)
        except Exception as e:
            self.logger.info(f"Exception Occurred in InputElements : {str(e.__cause__)}")
            self.takescreenshot.take_screenshot("Exception Occurred in ClickOnElements")

    def clear_value_from_textbox(self, web_element, field_name):
        try:
            common_utils = CommonUtils(self.driver)
            element_present = common_utils.check_presence_of_element(web_element)
            if element_present:
                web_element.clear()
                self.logger.info(f"Cleared value from text field : {field_name} ")
                self.takescreenshot.take_screenshot(f"Cleared value from text field : {field_name} ")
                time.sleep(5)
            else:
                web_element.clear()
                self.logger.info(f"Cleared value from text field : {field_name} ")
                self.takescreenshot.take_screenshot(f"Cleared value from text field : {field_name} ")
                time.sleep(5)
        except Exception as e:
            self.logger.info(f"Exception Occurred in InputElements : {str(e.__cause__)}")
            self.takescreenshot.take_screenshot("Exception Occurred in ClickOnElements")
