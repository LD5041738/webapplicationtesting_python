from elements.ClickOnElements import ClickOnElements
from elements.InputElements import InputElements
from pageObject.BasePage import BasePage
from utilities.customLogger import LogGen


class LoginPage(BasePage):
    logger = LogGen.get_logger("Login_Page")

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver
        self.login_page_locators = self.pageLocators('LoginPage')

    def set_username(self, field_name, actual_value):
        input_element = InputElements(self.driver)
        base_page = BasePage(self.driver)
        web_element = base_page.get_element(self.locator(self.login_page_locators, 'textbox_username_id'))
        web_element.clear()
        input_element.enter_value_in_textbox(web_element, field_name, actual_value)

    def set_password(self, field_name, actual_value):
        input_element = InputElements(self.driver)
        base_page = BasePage(self.driver)
        web_element = base_page.get_element(self.locator(self.login_page_locators, 'textbox_password_id'))
        web_element.clear()
        input_element.enter_value_in_textbox(web_element, field_name, actual_value)

    def click_login_button(self, button_name):
        click_on_element = ClickOnElements(self.driver)
        base_page = BasePage(self.driver)
        web_element = base_page.get_element(self.locator(self.login_page_locators, 'button_login_xpath'))
        click_on_element.click_on_element(web_element, button_name)

    def click_logout_button(self, button_name):
        click_on_element = ClickOnElements(self.driver)
        base_page = BasePage(self.driver)
        web_element = base_page.get_element(self.locator(self.login_page_locators, 'link_logout_link_text'))
        click_on_element.click_on_element(web_element, button_name)
