from utilities.customLogger import LogGen
from elements.SelectElements import SelectElements


class DashboardPage:
    logger = LogGen.get_logger("Dashboard_Page")

    def __init__(self, driver):
        self.driver = driver

    def select_from_menu(self, actual_value):
        select_element = SelectElements(self.driver)
        web_element = self.driver.find_element_by_xpath(
            "(//nav[@class='mt-2']//ul//li//a//p[contains(text(),'" + actual_value + "')])[1]")
        select_element.select_web_element(web_element, actual_value)

    def select_from_submenu(self, actual_value):
        select_element = SelectElements(self.driver)
        web_element = self.driver.find_element_by_xpath(
            "//li[@class='nav-item has-treeview menu-open']//ul//li//a//p[contains(text(),'" + actual_value + "')]")
        select_element.select_web_element(web_element, actual_value)


