from traceback import print_stack
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import *
from pageObject.BasePage import BasePage
from utilities.customLogger import LogGen


class Waits:
    logger = LogGen.get_logger("Waits")
    def __init__(self, driver):
        self.driver = driver

    # An expectation for checking that an element is present on the DOM of a page.
    # This does not necessarily mean that the element is visible.
    def wait_for_presence_of_element_located(self, web_element):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_element_located((By.XPATH, web_element)))

    # An expectation for checking the title of a page.
    # title is the expected title, which must be an exact match
    def wait_for_title(self, expected_title):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.title_is(expected_title))

    # An expectation for checking that the title contains a case-sensitive substring.
    def wait_for_title_contains(self, expected_title):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.title_contains(expected_title))

    # An expectation for checking that an element is present on the DOM of a page and visible.
    # Visibility means that the element is not only displayed but also has a height and width that is greater than 0.
    def wait_for_visibility_of_element_located(self, web_element):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.visibility_of_element_located((By.XPATH, web_element)))

    def wait_for_visibility_of(self, web_element):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.visibility_of((By.XPATH, web_element)))

    # An expectation for checking that there is at least one element present on a web page.
    # locator is used to find the element returns the list of WebElements once they are located
    def wait_for_presence_of_all_elements_located(self, web_element):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_all_elements_located((By.XPATH, web_element)))

    # An expectation for checking if the given text is present in the specified element. locator, text
    def wait_for_text_to_be_present_in_element(self, web_element, expected_text):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.text_to_be_present_in_element((By.XPATH, web_element, expected_text)))

    # An Expectation for checking that an element is either invisible or not present on the DOM.
    # locator used to find the element
    def wait_for_invisibility_of_element_located(self, web_element):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.invisibility_of_element_located((By.XPATH, web_element)))

    # An Expectation for checking an element is visible and enabled such that you can click it.
    # element is either a locator (text) or an WebElement
    def wait_for_element_to_be_clickable(self, web_element):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable((By.XPATH, web_element)))

    def wait_for_alert_is_present(self):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.alert_is_present())

    # An expectation for checking whether the given frame is available to switch to.
    # If the frame is available it switches the given driver to the specified frame.
    def wait_for_frame_to_be_available_and_switch_to_it(self, web_element):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.frame_to_be_available_and_switch_to_it((By.XPATH, web_element)))

    def wait_for_element(self, locator, locatorType = 'id', timeout = 10, pollFrequency = 0.5 ):
        base_page = BasePage(self.driver)
        element = None
        try:
            self.logger.info("Waiting for maximum :: " + str(timeout) + " :: seconds for element to be clickable")

            wait = WebDriverWait(self.driver, timeout, poll_frequency=pollFrequency,
                                 ignored_exceptions=[NoSuchElementException,
                                                     ElementNotVisibleException,
                                                     ElementNotSelectableException])
            ByType = base_page.getByType(locatorType)
            element = wait.until(expected_conditions.element_to_be_clickable((ByType,locator)))

            self.logger.info("Element appeared on the web page")

        except:
            self.logger.info("Element not appeared on the web page")
            print_stack()

        return element

    def page_has_loaded(self):
        try:
            WebDriverWait(self.driver, 1000, poll_frequency=0.5).until(lambda driver: self.driver.execute_script('return document.readyState == "complete";'))
            WebDriverWait(self.driver, 1000, poll_frequency=0.5).until(lambda driver: self.driver.execute_script('return jQuery.active == 0'))
            WebDriverWait(self.driver, 1000, poll_frequency=0.5).until(lambda driver: self.driver.execute_script('return typeof jQuery != "undefined"'))
            WebDriverWait(self.driver, 1000, poll_frequency=0.5).until(lambda driver: self.driver.execute_script('return angular.element(document).injector().get("$http").pendingRequests.length === 0'))
        except:
            return False
