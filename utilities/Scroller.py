from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class Scroller:

    def __init__(self, driver):
        self.driver = driver

    def scroll_by_python_function_by_xpath(self, web_element):
        element = self.driver.find_element(By.XPATH, web_element)
        var = element.location_once_scrolled_into_view

    def scroll_to_bottom_by_html_tag(self):
        html = self.driver.find_element_by_tag_name('html')
        html.send_keys(Keys.PAGE_DOWN)

    def scroll_to_element_by_id(self, id):
        self.driver.execute_script("document.getElementById('" + id + "').scrollIntoView();")

    def web_scroll(self, direction="up"):
        if direction == "up":
            # Scroll Up
            self.driver.execute_script("window.scrollBy(0, -1000);")
        if direction == "down":
            # Scroll Down
            self.driver.execute_script("window.scrollBy(0, 1000);")


