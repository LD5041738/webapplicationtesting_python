import os
from datetime import datetime
from _pytest.outcomes import xfail
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.edge.service import Service
from selenium.webdriver.ie.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.microsoft import IEDriverManager
from utilities.customLogger import LogGen
import pytest
from selenium.webdriver.chrome.options import Options

logger = LogGen.get_logger("conftest")


# This is used to open the browser.This acts like beforesuite
@pytest.fixture()
def setup(browser, request):
    if browser == "IE":
        driver = webdriver.Ie(executable_path=IEDriverManager().install())
        driver.maximize_window()
        driver.implicitly_wait(2)
        logger.info("Launching IE browser...")
    elif browser == "Firefox":
        driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))
        driver.maximize_window()
        driver.implicitly_wait(2)
        logger.info("Launching firefox browser...")
    elif browser == "Edge":
        driver = webdriver.Edge(executable_path=EdgeChromiumDriverManager().install())
        driver.maximize_window()
        driver.implicitly_wait(2)
        logger.info("Launching edge browser...")
    elif browser == "Chrome":
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.maximize_window()
        driver.implicitly_wait(2)
        logger.info("Launching chrome browser...")
    elif browser == "ChromeHeadless":
        options = Options()
        options.headless = True
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)
        driver.implicitly_wait(2)
        logger.info("Launching chrome browser in headless modes...")
    else:
        logger.info(f"{browser} browser not found. Please enter either of these choices : Chrome, Firefox, Edge, IE")
    request.cls.driver = driver
    yield driver
    driver.quit()


# This will get value from CLI/hook
def pytest_addoption(parser):
    parser.addoption("--browser")


# this will return browser value to setup method
@pytest.fixture()
def browser(request):
    return request.config.getoption("--browser")


############### PyTest HTML Report ##################################
# this hook will add environment value to html report
def pytest_configure(config):
    config._metadata['Project Name'] = "UIAutomationFramework"
    config._metadata['Test Triggred By'] = "Loveen Dwivedi"


# this hook will delete environment value to html report
@pytest.mark.optionalhook
def pytest_metadata(metadata):
    metadata.pop("JAVA_HOME", None)
    metadata.pop("Plugins", None)


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    timestamp = datetime.now().strftime('%H_%M_%S')
    pytest_html = item.config.pluginmanager.getplugin("html")
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, "extra", [])
    if report.when == "call" or report.when == "passed" or report.failed or report.skipped or report.when == "teardown":
        if (report.skipped and xfail) or (report.failed and not xfail) or (report.when == 'call') or (report.when == 'teardown'):
            feature_request = item.funcargs["request"]
            driver = feature_request.getfixturevalue("setup")
            img_name = "name_"+timestamp+".png"
            test_name = report.nodeid
            img_path = os.path.join(".\\Reports\\screenshots\\", img_name)
            driver.save_screenshot(img_path)
            if img_path:
                extra.append(pytest_html.extras.html('<div>Additional HTML</div>'))
                extra.append(pytest_html.extras.html(
                    f'<div class="image">'
                    f'<h2>Screenshot</h2>'
                    f'<p>Test name : {test_name}</p>'
                    f'<a href="screenshots/{img_name}">{test_name}'
                    f'</a>'
                    f'</div>'
                                                    )
                            )
            report.extra = extra