import allure
import pytest
from pageObject.CustomerPage import CustomerPage
from pageObject.DashboardPage import DashboardPage
from pageObject.LoginPage import LoginPage
from testCases.BaseTest import BaseTest
from utilities.Waits import Waits
from utilities.customLogger import LogGen
from utilities.readProperties import ReadConfig


#pytest -v -m "sanity or regression" --html=./Reports/report.html --browser=Chrome
class TestCustomer(BaseTest):
    base_url = ReadConfig.get_application_url()
    username = ReadConfig.get_application_username()
    password = ReadConfig.get_application_password()
    logger = LogGen.get_logger("Test_Customer")

    @pytest.mark.sanity
    @pytest.mark.regression
    @allure.story('epic_1')  # epic/story of the test case
    @allure.severity(allure.severity_level.MINOR)  # severity of the test case
    def test_add_customer_flow(self):
        try:
            with allure.step('Hitting URI'):
                self.driver.get(self.base_url)
                self.logger.info("URL hit :" + self.base_url)
                login_page = LoginPage(self.driver)
                login_page.set_username("Username", self.username)
                login_page.set_password("Password", self.password)
                login_page.click_login_button("Login")
                actual_title = self.driver.title

                wait = Waits(self.driver)
                wait.wait_for_title('Dashboard / nopCommerce administration')

                dashboard_page = DashboardPage(self.driver)
                dashboard_page.select_from_menu("Customers")
                dashboard_page.select_from_submenu("Customers")

                customer_page = CustomerPage(self.driver)
                customer_page.click_add_new_button("Add New")
                customer_page.set_value("Email", "slove@gmail.com")
                customer_page.set_value("Password", "slove@gmail.com")
                customer_page.set_value("First name", "slove@gmail.com")
                customer_page.set_value("Last name", "slove@gmail.com")
                customer_page.select_radio_button("Male")
                customer_page.set_value("Date of birth", "1/1/1995")
                customer_page.set_value("Company name", "test")
                customer_page.set_value("Is tax exempt", "Is tax exempt")
        except Exception as e:
            print("Exception Occurred :"+str(e))

