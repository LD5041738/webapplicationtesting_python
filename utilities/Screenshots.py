import os
from datetime import datetime
from traceback import print_stack
import time
import allure
from utilities.customLogger import LogGen

nowDateTime = datetime.now()
class Screenshots:
    logger = LogGen.get_logger("Screenhots")

    def __init__(self, driver):
        self.driver = driver

    def directory_is_present(self, path_of_directory):
        if(os.path.exists(path_of_directory)):
            pass
        else:
            os.makedirs(path_of_directory)

    def take_screenshot(self, resultMessage):
        """
        Take a screenshot of the current open web page
        """
        fileName = resultMessage + "." + str(round(time.time() *1000)) + ".png"
        #if len(fileName) >= 200:
            #fileName = str(round(time.time() *1000)) + ".png"
        screenshotDirectory = "..\\Screenshots\\WebApplicationTesting_"+ str(nowDateTime.strftime("%d_%m_%Y-%H_%M_%S"))+"\\"
        self.directory_is_present(screenshotDirectory)
        relativeFileName = screenshotDirectory + fileName
        currentDirectory = os.path.dirname(__file__)
        destinationFile = os.path.join(currentDirectory, relativeFileName)
        destinationDirectory = os.path.join(currentDirectory, screenshotDirectory)

        try:
            if not os.path.exists(destinationDirectory):
                os.makedirs(destinationDirectory)
            self.driver.save_screenshot(destinationFile)
            allure.attach(self.driver.get_screenshot_as_png(),
                          name=fileName,
                          attachment_type=allure.attachment_type.PNG)
            self.logger.info("Screenshot save to directory: " + destinationFile)
        except:
            self.logger.error("Exception Occurred when taking screenshot")
            print_stack()