import time

from utilities.Screenshots import Screenshots
from utilities.customLogger import LogGen
from utilities.commonUtils import CommonUtils


class RadioButton:
    logger = LogGen.get_logger("RadioButton")

    def __init__(self, driver):
        self.driver = driver
        self.takescreenshot = Screenshots(driver)

    def select_radio_button(self, web_element, actual_value):
        try:
            common_utils = CommonUtils(self.driver)
            element_present = common_utils.check_presence_of_element(web_element)
            if element_present:
                web_element.click()
                self.logger.info(f"{actual_value} is selected")
                self.takescreenshot.take_screenshot(f"{actual_value} is selected")
                time.sleep(5)
            else:
                time.sleep(5)
                web_element.send_keys(actual_value)
                self.logger.info(f"{actual_value} is selected")
                self.takescreenshot.take_screenshot(f"{actual_value} is selected")
                time.sleep(5)
        except Exception as e:
            self.logger.info(f"Exception Occurred in RadioButton : {str(e.__cause__)}")
            self.takescreenshot.take_screenshot("Exception Occurred in ClickOnElements")
