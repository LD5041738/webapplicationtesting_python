import os
from traceback import print_stack
from selenium.webdriver.common.by import By
import utilities.read_json as RJ
from utilities.customLogger import LogGen


class BasePage:
    logger = LogGen.get_logger("Base_Page")

    def __init__(self, driver):
        self.driver = driver

    def pageLocators(self, page):
        """
        read the locators of specific page
        :param page: page
        :return: list of all locators in specific page
        """
        locatorsPath = os.path.abspath("./locators/locators.json")
        locatorsJsonFile = RJ.readJson(locatorsPath)
        pageLocators = [locator for locator in locatorsJsonFile if locator['pageName'] in page]
        return pageLocators

    def locator(self, pageLocators, locatorName):
        """
        get specific locator in specific page
        :param pageLocators: specific page
        :param locatorName: locator name
        :return: locator and locator Type
        """
        for locator in pageLocators:
            if locatorName == locator['name']:
                return locator['locator'], locator['locateUsing']

    def verifyPageTitle(self, titleToVerify):
        """
        Verify the page Title

        :param titleToVerify: Title on the page that needs to be verified
        """
        try:
            actualTitle = self.getTitle()
            return  self.util.verifyTextContains(actualTitle, titleToVerify)
        except:
            self.logger.error("Failed to get page title")
            print_stack()
            return False

    def get_element(self, locators):
        element = None
        try:
            locator, locator_type = locators
            locator_type = locator_type.lower()
            byType = self.getByType(locator_type)
            element = self.driver.find_element(byType, locator)
            self.logger.info("Element found with locator: " + locator +
                          " and locatorType: " + locator_type)
        except Exception as e:
            self.logger.error("Element not found with locator: " + locator +
                          " and locatorType: " + locator_type)
        return element

    def getByType(self, locatorType):
        locatorType = locatorType.lower()
        if locatorType == "id":
            return By.ID
        elif locatorType == "name":
            return By.NAME
        elif locatorType == "xpath":
            return By.XPATH
        elif locatorType == "css":
            return By.CSS_SELECTOR
        elif locatorType == "class":
            return By.CLASS_NAME
        elif locatorType == "link_text":
            return By.LINK_TEXT
        else:
            self.logger.info("Locator type" + locatorType + "not correct/supported")
        return False
