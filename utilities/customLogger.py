import logging
import sys
from datetime import datetime
from logging.handlers import TimedRotatingFileHandler

nowDateTime = datetime.now()
FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")
LOG_FILE = ".\\Logs\\UIAutomationFramework_" + str(nowDateTime.strftime("%d_%m_%Y-%H_%M_%S"))+".log"


class LogGen:

    @staticmethod
    def get_console_handler():
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(FORMATTER)
        return console_handler

    @staticmethod
    def get_file_handler():
        file_handler = TimedRotatingFileHandler(LOG_FILE)
        file_handler.setFormatter(FORMATTER)
        return file_handler

    @staticmethod
    def get_logger(logger_name):
        logger = logging.getLogger(logger_name)
        logger.setLevel(logging.INFO)  # better to have too much log than not enough
        logger.addHandler(LogGen.get_console_handler())
        logger.addHandler(LogGen.get_file_handler())
        # with this pattern, it's rarely necessary to propagate the error up to parent
        logger.propagate = False
        return logger
